package debl.analysis;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SummariseStaticAnalysisResults {

    public static final File DATA_FOLDER = new File("doopoutput");
    public static final File OUTPUT_FOLDER = new File("out");
    public static final Logger LOGGER = Logger.getLogger("summarise");

    static class Record {
        public Record(String program, String analysis) {
            this.program = program;
            this.analysis = analysis;
        }

        String program = null;
        String analysis = null;
        Map<String,Integer> metrics = new HashMap<>();
    }

    static class Table {
        List<String> headers = new ArrayList<>();
        List<List<String>> rows = new ArrayList<>();
    }

    public static void main (String[] args) throws Exception {

        BasicConfigurator.configure();

        Preconditions.checkState(DATA_FOLDER.exists());
        Set<Record> records = new HashSet<>();

        for (File data:DATA_FOLDER.listFiles(n -> n.getName().endsWith(".stats"))) {
            String[] tokens = data.getName().replace(".stats","").split("-");
            String program = tokens[0].trim();
            String transformed = tokens[1].trim();
            String analysis = "";
            for (int i=2;i<tokens.length;i++) {
                if (!analysis.isEmpty()) analysis = analysis + "-";
                analysis = analysis + tokens[i];
            }

            File runtime = new File(data.getAbsolutePath().replace("stats","runtime"));

            // parse
            List<String> statsLines = Files.readLines(data, Charset.defaultCharset());
            List<String> runtimeLines = runtime.exists()?Files.readLines(runtime, Charset.defaultCharset()):null;

            if (statsLines.isEmpty()) {
                LOGGER.warn("Empty stats file, will ignore : " + data.getName());
            }
            else if (runtimeLines.isEmpty()) {
                LOGGER.warn("Empty runtime file, will ignore : " + data.getName());
            }
            else if (!runtime.exists()) {
                LOGGER.warn("Runtime data file does not exists: " + runtime.getAbsolutePath());
            }
            else if (check4Error(data,statsLines,runtimeLines)) {
                LOGGER.warn("Analysis error, will ignore : " + data.getName());
            }
            else {
                statsLines = removePreamble(statsLines);
                runtimeLines = removePreamble(runtimeLines);

                analysis = transformed.equals("transformed") ? "transformed-"+analysis : analysis;

                Record record = new Record(program,analysis);
                for (String line:statsLines) {
                    line = line.trim();
                    if (line.length()>0) {
                        String[] tok = line.split(",");
                        assert tok.length == 3;
                        record.metrics.put(tok[1].trim(), Integer.parseInt(tok[2].trim()));
                    }
                }
                for (String line:runtimeLines) {
                    line = line.trim();
                    if (line.length()>0) {
                        String[] tok = line.split(",");
                        assert tok.length == 2;
                        record.metrics.put(tok[0].trim(), Integer.parseInt(tok[1].trim()));
                    }
                }
                records.add(record);

            }
        }

        // print records for a certain metric
        OUTPUT_FOLDER.mkdirs();

        renderAsCSV(new File(OUTPUT_FOLDER,"points-to.csv"),createTable(records,"var points-to (INS)"));

    }

    private static void renderAsCSV(File file, Table table) throws IOException {
        try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
            out.println(table.headers.stream().collect(Collectors.joining("\t")));
            for (List<String> row:table.rows) {
                out.println(row.stream().collect(Collectors.joining("\t")));
            }
        }

        LOGGER.info("Table written to " + file.getAbsolutePath());
    }

    private static Table createTable(Set<Record> records, String metric) {
        Table table = new Table();
        List<String> programs = records.stream().map(r -> r.program).distinct().sorted().collect(Collectors.toList());
        List<String> analyses = records.stream().map(r -> r.analysis).distinct().sorted().collect(Collectors.toList());
        table.headers.add("program");
        table.headers.addAll(analyses);
        for (String program:programs) {
            List<String> row = new ArrayList<>();
            row.add(program);
            for (String analysis:analyses) {
                Optional<Record> record = records.stream().filter(r -> r.program.equals(program) && r.analysis.equals(analysis)).findAny();
                if (record.isPresent()) {
                    Integer value = record.get().metrics.get(metric);
                    assert value != null;
                    row.add(format(value));
                }
                else {
                    LOGGER.warn("No metrics found for program " + program + " / analysis " + analysis);
                }
            }
            table.rows.add(row);
        }
        return table;
    }

    private static String format(int v) {
        return ""+v;
    }

    private static List<String> removePreamble(List<String> lines) {
        List<String> cleaned = new ArrayList<>();
        boolean preamble = true;
        for (String line:lines) {
            if (preamble && line.startsWith("/---")) {
                preamble = false;
            }
            else if (!preamble && !line.startsWith("\\---")) {
                cleaned.add(line);
            }
        }
        return cleaned;

    }

    private static final Pattern WARNPattern = Pattern.compile(".*WARN.*Warning:.*Transaction.*aborted.*");
    private static boolean check4Error(File data, List<String> lines1,List<String> lines2) {
        return WARNPattern.matcher(lines1.get(0)).matches() || WARNPattern.matcher(lines2.get(0)).matches();
    }
}
