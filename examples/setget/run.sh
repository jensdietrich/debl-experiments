#!/bin/bash

# project-specific parameters
main=debl.setget.Main
apppackage=debl.setget.*
jarname=setget-1.0.0.jar
varpattern='<debl.setget.Main: void main(java.lang.String[])>/obj%';
# derived, by convention
jarfolder=$(pwd)/target
jartransformedfolder=$(pwd)/transformed
jar=$jarfolder/$jarname
jartransformed=$jartransformedfolder/$jarname
transformationlog=$jartransformedfolder/debl.log
debl=../../debl/debl-1.0-SNAPSHOT-jar-with-dependencies.jar
results=$(pwd)/results
resultsbasiccontextinsensitive=$results/basic-contextinsensitive.csv
resultsbasiccontextinsensitive_rt=$results/basic-contextinsensitive.rt
resultstransformedcontextinsensitive=$results/transformed-contextinsensitive.csv
resultstransformedcontextinsensitive_rt=$results/transformed-contextinsensitive.rt
resultsbasic1callsite=$results/basic-1callsiteheap.csv
resultsbasic1callsite_rt=$results/basic-1callsiteheap.rt

timestamp=$SECONDS
logruntime() {
   echo $(($SECONDS-$timestamp)) > $1
   timestamp=$SECONDS
}

echo "prepare"
mkdir $results
mkdir $jartransformedfolder

echo "building example"
mvn clean package

echo "debloat jar"

if [ -f "$debl" ]
then
	echo "$debl found"
else
	echo "$debl missing !"
	exit 1
fi

echo "main:" $main
echo "debl binary:" $debl
echo "jar:" $jar
echo "transformedjar:" $jartransformed
echo "log:" $transformationlog
java -jar $debl -in $jarfolder -out $jartransformedfolder -log $transformationlog

echo "perform doop context-insensitive analysis"
if [[ -z "${DOOP_HOME}" ]]; then
  echo "DOOP_HOME is undefined"
  exit 1
fi
cd $DOOP_HOME
echo "changed working folder to:" $(pwd)
# next line is to make sure logicblox finds required libs
source /opt/lb/pa-datalog/lb-env-bin.sh

# context-insensitive analysis original jar
timestamp=$SECONDS
./doopOffline --platform java_7 -a context-insensitive --lb -i $jar --main $main --regex $apppackage
bloxbatch -db last-analysis -query "_p2(?var,?hobj) <- VarPointsTo(_,?id,_,?var),Value:Id(?id,?hobj),string:like(?var,\"$varpattern\")." > $resultsbasiccontextinsensitive
logruntime $resultsbasiccontextinsensitive_rt

# context-insensitive analysis transformed jar
timestamp=$SECONDS
./doopOffline --platform java_7 -a context-insensitive --lb -i $jartransformed --main $main --regex $apppackage
bloxbatch -db last-analysis -query "_p2(?var,?hobj) <- VarPointsTo(_,?id,_,?var),Value:Id(?id,?hobj),string:like(?var,\"$varpattern\")." > $resultstransformedcontextinsensitive
logruntime $resultstransformedcontextinsensitive_rt

# context-sensitive analysis original jar
timestamp=$SECONDS
./doopOffline --platform java_7 -a 1-call-site-sensitive+heap --lb -i $jar --main $main --regex $apppackage
bloxbatch -db last-analysis -query "_p2(?var,?hobj) <- VarPointsTo(_,?id,_,?var),Value:Id(?id,?hobj),string:like(?var,\"$varpattern\")." > $resultsbasic1callsite
logruntime $resultsbasic1callsite_rt

