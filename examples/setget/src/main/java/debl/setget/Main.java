package debl.setget;
public class Main {
    public static void main(String[] args) {
        Object obj1a = new Object();
        Object obj2a = new Object();
        Bean bean1 = new Bean();
        Bean bean2 = new Bean();
        bean1.setF(obj1a);
        bean2.setF(obj2a);
        Object obj1b = bean1.getF();
        Object obj2b = bean2.getF();
    }

}