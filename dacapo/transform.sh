#!/bin/bash


transformationlog=debl.log
debl=../debl/debl-1.0-SNAPSHOT-jar-with-dependencies.jar

echo "debloating dacapo jars"

if [ -f "$debl" ]
then
	echo "$debl found"
else
	echo "$debl missing !"
	exit 1
fi

java -jar $debl -in $(pwd)/dacapo2009/ -out $(pwd)/transformed/ -log $transformationlog


