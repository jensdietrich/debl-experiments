#!/bin/bash

results=$(pwd)/results
dacapofolder=$(pwd)/dacapo2009
#dacapoprograms=(avrora)
dacapoprograms=(avrora batik)

mkdir $results
if [[ -z "${DOOP_HOME}" ]]; then
  echo "DOOP_HOME is undefined"
  exit 1
fi
cd $DOOP_HOME
# next line is to make sure logicblox finds required libs
source /opt/lb/pa-datalog/lb-env-bin.sh

for program in "${dacapoprograms[@]}"
do
   # cleaning up doop to make sure there is anothe space
   #rm -r ${DOOP_HOME}/cache/*
   #rm -r ${DOOP_HOME}/out/*
   #rm -r ${DOOP_HOME}/result/*
   jar=${dacapofolder}/${program}".jar"
   jardep=${dacapofolder}/${program}"-deps.jar"
   stats=${results}/${program}-ctxinsens.stats
   runtime=${results}/${program}-ctxinsens.runtime
   out=${results}/${program}-ctxinsens.p2
   echo "analysing " ${jar} "," ${jardep}
   ./doopOffline --platform java_7 -id ${program} -a context-insensitive -t 120 -i $jar --lb --dacapo-bach
   db=${DOOP_HOME}/out/context-insensitive/${program}/database
   bloxbatch -db ${db} -print Stats:Metrics  > $stats
   bloxbatch -db ${db} -print Stats:Runtime > $runtime
   #echo  "analysis summary written to " ${stats}
   #echo  "performance data written to " ${runtime}
   bloxbatch -db ${db} -query "_p2(?var,?hobj) <- VarPointsTo(_,?id,_,?var),Value:Id(?id,?hobj)."  > $out
done
